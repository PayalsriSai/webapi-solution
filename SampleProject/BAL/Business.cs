﻿using CsvHelper;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using SampleProject.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace SampleProject.BAL
{
    public class Business
    {
        private string PathToConfigFolder;
        public List<Claim> ClaimArray { get; private set; }
        public List<Member> MemberArray { get; private set; }

        public Business(string rootPath)
        {
            PathToConfigFolder = rootPath + "\\" + "Files";
        }

        public void ReadCsv(string CsvFilePath, string MapperName)
        {
            using (var fileStream = new FileStream(CsvFilePath, FileMode.Open, FileAccess.Read))
            using (var reader = new StreamReader(fileStream))
            {
                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                {
                    if (MapperName == "Claim")
                    {
                        var records = csv.GetRecords<Claim>();
                        ClaimArray = records.ToList();
                    }
                    else
                    {
                        var records = csv.GetRecords<Member>();
                        MemberArray = records.ToList();
                    }
                }
            }
        }

        public List<Claim> GetInsuranceClaim()
        {
            List<Claim> claimList = new List<Claim>();
            string csvClaimPath = Path.Combine(PathToConfigFolder, "Claim.csv");
            ReadCsv(csvClaimPath,"Claim");

            return claimList = ClaimArray;
        }

        public List<Member> GetInsuramceMember()
        {
            List<Member> memberList = new List<Member>();
            string csvClaimPath = Path.Combine(PathToConfigFolder, "Member.csv");
            ReadCsv(csvClaimPath, "Member");

            return memberList = MemberArray;
        }
    }
}
