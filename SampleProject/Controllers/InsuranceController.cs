﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SampleProject.BAL;
using SampleProject.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleProject.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class InsuranceController : ControllerBase
    {
        public List<Claim> claimList = new List<Claim>();
        public List<Member> memberList = new List<Member>();
        private IHostingEnvironment _hostingEnvironment;

        public InsuranceController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
            Business business = new Business(_hostingEnvironment.ContentRootPath);
            claimList = business.GetInsuranceClaim();
            memberList = business.GetInsuramceMember();
        }

        //GET: api/TodoItems
        [HttpGet]
        public IEnumerable<InsuaranceData> GetTodoItems()
        {
            List<InsuaranceData> insuaranceData = new List<InsuaranceData>();

            foreach (var item in claimList)
            {
                var memberData = memberList.Where(m => m.MemberID == item.MemberID).Select(m => { return m; }).ToArray();
                insuaranceData.Add(new InsuaranceData()
                {
                    MemberID = item.MemberID,
                    EnrollmentDate = memberData.FirstOrDefault().EnrollmentDate,
                    FirstName = memberData.FirstOrDefault().FirstName,
                    LastName = memberData.FirstOrDefault().LastName,
                    ClaimAmount = item.ClaimAmount,
                    ClaimDate = item.ClaimDate.Date
                });
            }
            return insuaranceData;
        }

        [HttpGet("{claimDate}")]
        public IEnumerable<InsuaranceData> Get(DateTime claimDate)
        {
            List<InsuaranceData> insuaranceData = new List<InsuaranceData>();

            var claimData = claimList.Where(c => c.ClaimDate.Date <= Convert.ToDateTime(claimDate).Date).Select(c => { return c; }).ToArray();

            claimList.Where(c => c.ClaimDate.Date <= Convert.ToDateTime(claimDate).Date).ToList().ForEach(a =>
            {
                var memberData = memberList.Where(m => m.MemberID == a.MemberID).Select(m => { return m; }).ToList();
                insuaranceData.Add(new InsuaranceData()
                {
                    MemberID = a.MemberID,
                    EnrollmentDate = memberData.FirstOrDefault().EnrollmentDate,
                    FirstName = memberData.FirstOrDefault().FirstName,
                    LastName = memberData.FirstOrDefault().LastName,
                    ClaimAmount = a.ClaimAmount,
                    ClaimDate = a.ClaimDate.Date
                });
            });

            return insuaranceData;
        }
    }
}
